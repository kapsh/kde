# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge qmake [ slot=5 ] freedesktop-desktop gtk-icon-cache

export_exlib_phases src_prepare pkg_postinst pkg_postrm

SUMMARY="A great media player"
DESCRIPTION="
SMPlayer is a graphical user interface (GUI) for the award-winning mplayer and also for mpv. But
apart from providing access for the most common and useful options of mplayer and mpv, SMPlayer
adds other interesting features like the possibility to play Youtube videos or search and download
subtitles. One of the main features is the ability to remember the state of a played file, so when
you play it later it will be resumed at the same point and with the same settings.
"

LICENCES="GPL-2 WTFPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        x11-libs/qttools:5 [[ note = [ lrelease/lconvert for translations ] ]]
    build+run:
        x11-libs/libX11
        x11-libs/qtbase:5[gui]
        x11-libs/qtscript:5
    run:
        media/mpv[>=0.15.0]
    suggestion:
        net-misc/youtube-dl [[ description = [ Adds playback support for additional video websites ] ]]
        x11-themes/smplayer-themes [[ description = [ Additional SMPlayer GUI themes ] ]]
"

EQMAKE_SOURCES=( src/${PN}.pro )
EQMAKE_PARAMS=(
    -o src/Makefile
    CONFIG-="UPDATE_CHECKER"
)

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-16.8.0-makefile.patch
)

DEFAULT_SRC_COMPILE_PARAMS=(
    PREFIX=/usr/$(exhost --target)
)

DEFAULT_SRC_INSTALL_PARAMS=(
    DOC_PATH=/usr/share/doc/${PNVR}
    PREFIX=/usr/$(exhost --target)
)

smplayer_src_prepare() {
    edo lrelease-qt5 src/translations/${PN}_*.ts

    edo sed \
        -e 's:QMAKE=qmake:QMAKE=qmake-qt5:g' \
        -e 's:LRELEASE=lrelease:LRELEASE=lrelease-qt5:g' \
        -i Makefile

    default
}

smplayer_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

smplayer_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

