# Copyright 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=googlei18n tag=v${PV} ] cmake

export_exlib_phases src_test

SUMMARY="Google's library for parsing, formatting, and validating international phone numbers"

LICENCES="Apache-2.0"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-cpp/gtest
        dev-libs/boost[>=1.40.0]
        dev-libs/icu:=[>=4.4]
        dev-libs/protobuf:=[>=2.4]
"

CMAKE_SOURCE="${WORKBASE}"/${PNV}/cpp

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_STATIC_LIB=OFF
    # Would need a JRE, use the metadata from the source tree instead
    -DREGENERATE_METADATA:BOOL=OFF
    -DUSE_BOOST=ON
    # Required for libphonenumber to be thread-safe
    -DUSE_STDMUTEX=ON
    # Despite being also written by Google, it fails to build when enabled
    -DUSE_RE2=OFF
)

libphonenumber_src_test() {
    edo ./libphonenumber_test
}

