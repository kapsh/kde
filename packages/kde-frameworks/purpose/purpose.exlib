# Copyright 2015-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks [ docs=false ] kde [ translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="Offers available actions for a specific purpose"
DESCRIPTION="
This framework offers the possibility to create integrate services and actions
on any application without having to implement them specifically. Purpose will
offer them mechanisms to list the different alternatives to execute given the
requested action type and will facilitate components so that all the plugins
can receive all the information they need."

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    GPL-2
"
SLOT="5"
MYOPTIONS="
    kaccounts [[ description = [
        Use system-wide defined accounts to share media (Twitter and YouTube for now)
    ] ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool
        virtual/pkg-config
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=5.2.0]
        x11-libs/qtdeclarative:5[>=5.2.0]
        kaccounts? ( kde/kaccounts-integration:4 )
    run:
        kde-frameworks/kdeclarative:5   [[ note = [ kquickcontrolsaddons ] ]]
        x11-libs/qtquickcontrols:5[>=5.2.0]
        x11-libs/qtquickcontrols2:5[>=5.2.0]
    suggestion:
        app-mobilephone/kdeconnect [[ description = [ Send stuff to your phone via kdeconnect ] ]]
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( KAccounts )

# TODO: Wants to start several ioslaves, needs figuring out if that can and
# should be whitelisted.
RESTRICT="test"

purpose_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop
}

