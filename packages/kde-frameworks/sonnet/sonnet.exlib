# Copyright 2014-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde [ translations='qt' ]

SUMMARY="A plugin-based spell-checking framework for Qt-base applications"
DESCRIPTION="
It supports several different plugins, including HSpell, Enchant, ASpell and
HUNSPELL.
It also supports automated language detection, based on a
combination of different algorithms."

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] LGPL-2.1"
MYOPTIONS="
    aspell   [[ description = [ Use aspell as a spelling backend ] ]]
    designer [[ description = [ Install Qt designer plugins ] ]]
    hunspell [[ description = [ Use hunspell as a spelling backend ] ]]
    hspell   [[ description = [ Use hpell as a spelling backend for Hebrew ] ]]
    voikko   [[ description = [ Build a backend for Voikko (spelling and grammar checker for the Finnish language) ] ]]

    ( aspell hunspell hspell voikko ) [[ number-selected = at-least-one ]]"
#    enchant [[ description = [ Use enchant as a spelling backend ] ]]

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        aspell? ( app-spell/aspell )
        designer? ( x11-libs/qttools:5[>=${QT_MIN_VER}] )
        hunspell? ( app-spell/hunspell:= )
        hspell? (
            app-spell/hspell
            sys-libs/zlib
        )
        voikko? ( app-spell/voikko )
"

CMAKE_SRC_CONFIGURE_PARAMS+=( -DSONNET_USE_WIDGETS:BOOL=TRUE )

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=(
    'designer DESIGNERPLUGIN'
)

# enchant backend is currently disabled by upstream, because it's buggy
#CMAKE_SRC_CONFIGURE_PARAMS+=( -DCMAKE_DISABLE_FIND_PACKAGE_ENCHANT:BOOL=TRUE )
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    ASPELL
    HUNSPELL
    HSPELL
    VOIKKO
)

# Disable tests which cause problems because of C locale
DEFAULT_SRC_TEST_PARAMS+=( ARGS+="-E '(sonnet-test_autodetect|sonnet-test_settings)'" )

