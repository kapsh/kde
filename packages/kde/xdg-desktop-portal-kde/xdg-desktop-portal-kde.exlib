# Copyright 2017-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]

SUMMARY="A KDE implementation of Flatpak portals backend"
DESCRIPTION="
xdg-desktop-portal calls desktop specific implementations to present native
dialogs and have proper integration for sandboxed applications like Flatpak
ones. This is a backend for KDE's Plasma."

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    LGPL-2.1
"
SLOT="0"
MYOPTIONS="
    screencast [[ description = [ Portal which supports screen sharing ] ]]
"

KF5_MIN_VER="5.66.0"
if ever at_least 5.18.90 ; then
    QT_MIN_VER="5.14.0"
else
    QT_MIN_VER="5.12.0"
fi

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwayland:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][cups]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        screencast? (
            dev-libs/libepoxy
            media/pipewire[>=0.2] [[ note = [ libpipewire and libspa ] ]]
            x11-dri/mesa [[ note = gbm ]]
        )
    run:
        sys-apps/flatpak
        sys-apps/xdg-desktop-portal
        x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
"

if ever at_least 5.18.90 ; then
    CMAKE_SRC_CONFIGURE_OPTION_ENABLES+=( 'screencast PIPEWIRE' )
else
    CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
        'screencast Epoxy'
        'screencast GBM'
        'screencast PipeWire'
    )
fi

