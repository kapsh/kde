# Copyright 2012 Bo Ørsted Andresen
# Copyright 2014 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='qt' ]
require xdummy [ phase=test ]

export_exlib_phases src_prepare src_test

SUMMARY="Library for adding mathematical features to your program"

LICENCES="FDL-1.2 GPL-2 LGPL-2 BSD-2 [[ note = [ FindEigen3.cmake ] ]]"
SLOT="5"
MYOPTIONS=""

QT_MIN_VER="5.6"

DEPENDENCIES="
    build:
        sci-libs/eigen:3
    build+run:
        x11-dri/mesa
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
"

analitza_src_prepare() {
    kde_src_prepare

    # Would be automagic otherwise, should use a proper find_package call.
    # A small demo app turning plots into soundwaves depends on it.
    edo sed \
        -e "/find_library(SNDFILE sndfile)/d" \
        -i analitzaplot/examples/CMakeLists.txt
}

analitza_src_test() {
    xdummy_start

    default

    xdummy_stop
}

