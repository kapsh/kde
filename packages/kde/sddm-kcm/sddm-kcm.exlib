# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations=ki18n ]

SUMMARY="KDE config module for SDDM"

LICENCES="GPL-2"
SLOT="4"
MYOPTIONS=""

KF5_MIN_VER="5.66.0"
if ever at_least 5.18.90 ; then
    QT_MIN_VER="5.14.0"
else
    QT_MIN_VER="5.12.0"
fi

DEPENDENCIES="
    build+run:
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kauth:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
    run:
        kde/kde-cli-tools:4 [[ note = [ kcmshell5 ] ]]
    recommendation:
        sys-auth/polkit-kde-agent[>=5.1.95] [[ description = [ Needed for saving settings requiring authentication ] ]]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DWAYLAND_SESSIONS_DIR=/usr/share/wayland-sessions
    -DXSESSIONS_DIR=/usr/share/xsessions
)

