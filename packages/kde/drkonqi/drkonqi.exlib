# Copyright 2017-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]

export_exlib_phases src_prepare

SUMMARY="KDE's crash handler"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="X"

if ever at_least 5.18.90 ; then
    KF5_MIN_VER=5.69.0
    QT_MIN_VER=5.14.0
else
    KF5_MIN_VER=5.61.0
    QT_MIN_VER=5.12.0
fi

DEPENDENCIES="
    build+run:
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidletime:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        X? ( x11-libs/qtx11extras:5[>=${QT_MIN_VER}] )
        !kde/plasma-workspace:4[<5.10.90] [[
            description = [ kde/drkonqi was split out from kde/plasma-workspace ]
            resolution = uninstall-blocked-after
        ]]
    run:
        sys-devel/gdb
"

if ever at_least 5.18.90 ; then
    DEPENDENCIES+="
        build+run:
            kde-frameworks/syntax-highlighting:5[>=${KF5_MIN_VER}]
    "
fi

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'X Qt5X11Extras' )

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # TODO: integration tests would need ruby, at-spi2-core, atspi and xmlrpc
    #       gems (both unwritten), gdb, and xvfb-run.
    -DWITH_DRKONI_INTEGRATION_TESTING:BOOL=FALSE
)

drkonqi_src_prepare() {
    kde_src_prepare

    # Disable test which needs dbus, runs kdeinit5 and wants to connect to
    # komaci.kde.org/. We can handle the former two, but not the latter.
    edo sed \
        -e "s/connectiontest.cpp/#&/" \
        -i src/bugzillaintegration/libbugzilla/autotests/CMakeLists.txt
}

