# Copyright 2016 Niels Ole Salscheider <olesalscheider@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps [ pn='pim-sieve-editor' ] kde [ translations='ki18n' ]
require xdummy [ phase=test ]

export_exlib_phases src_test

SUMMARY="KDE application to edit sieve scripts"

LICENCES="FDL-1.2 GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS+="
    userfeedback [[
        description = [ Allows sending anonymized usage information to KDE ]
    ]]
"

KF5_MIN_VER=5.71.0
QT_MIN_VER=5.13.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde/libksieve[>=${PV}]
        kde/pimcommon[>=${PV}]
        kde-frameworks/kbookmarks:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kimap:5[>=${PV}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmailtransport:5[>=${PV}]
        kde-frameworks/kpimtextedit:5[>=${PV}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        userfeedback? ( kde/kuserfeedback[>=0.9.90] )
    suggestion:
        kde/kdepim-addons:4 [[
            description = [ Provides a plugin for IMAP folder completion ]
        ]]
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'userfeedback KUserFeedback' )

sieveeditor_src_test() {
    xdummy_start

    default

    xdummy_stop
}

