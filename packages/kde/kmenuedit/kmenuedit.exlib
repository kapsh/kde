# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]
require gtk-icon-cache

SUMMARY="KDE Menu editor"
DESCRIPTION="
Allows editing one of the KDE application launchers: Kickoff, the classic KMenu or
Lancelot Launcher."

LICENCES="FDL-1.2 GPL-2"
SLOT="4"
MYOPTIONS=""

KF5_MIN_VER="5.66.0"
if ever at_least 5.18.90 ; then
    QT_MIN_VER="5.14.0"
else
    QT_MIN_VER="5.12.0"
fi

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/sonnet:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
"

if ever at_least 5.18.90 ; then
    :
else
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kinit:5[>=${KF5_MIN_VER}]
    "
fi

