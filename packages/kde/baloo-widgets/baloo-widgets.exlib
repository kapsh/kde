# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="Widgets providing file metadata for search-indexing"

LICENCES="
    GPL-2 [[ note = [ utils/daterange.h, not compiled or installed ] ]]
    LGPL-2.1
"
MYOPTIONS=""

KF5_MIN_VER="5.71.0"
QT_MIN_VER="5.11.0"

DEPENDENCIES+="
    build+run:
        kde-frameworks/baloo:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kfilemetadata:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
"

DEFAULT_SRC_TEST_PARAMS+=(
    # Skip tests which want access to the system dbus
    ARGS+="-E '(filemetadatawidgettest|filemetadataitemcounttest)'"
)

baloo-widgets_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop
}

