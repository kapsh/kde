# Copyright 2012, 2014 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2014-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde gtk-icon-cache

SUMMARY="KDE volume control program"
HOMEPAGE="
    https://www.kde.org/
    https://multimedia.kde.org/
"

LICENCES="GPL-2 LGPL-2.1 FDL-1.2  BSD-3 [[ note = [ cmake scripts ] ]]"
MYOPTIONS="
    canberra [[ description = [ Enable sound event support via libcanberra ] ]]
    pulseaudio"

KF5_MIN_VER="5.48.0"
QT_MIN_VER="5.11.0"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}] [[
            note = [ Could be optional starting with 19.11.80 ]
        ]]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        sys-sound/alsa-lib
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        canberra? (  media-libs/libcanberra )
        pulseaudio? ( media-sound/pulseaudio[>=0.9.16] )
    run:
        kde/kde-cli-tools:4 [[ note = [ kcmshell5 for 'Audio Setup' ] ]]
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    Canberra
    PulseAudio
)

