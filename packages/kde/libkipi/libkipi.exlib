# Copyright 2011 Timo Gurr <tgurr@exherbo.org>
# Copyright 2015 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde
require alternatives gtk-icon-cache

export_exlib_phases src_prepare src_install pkg_postinst pkg_postrm

SUMMARY="An interface to use kipi-plugins from a KDE image management program like digiKam"

LICENCES="GPL-2 LGPL-2 BSD-3 [[ note = [ cmake scripts ] ]]"
SLOT="5"
MYOPTIONS=" examples"

KF5_MIN_VER="5.1.0"

DEPENDENCIES+="
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=5.2.0]
        examples? (
            kde-frameworks/libkdcraw:5
            kde-frameworks/libkexiv2:5
        )
"
# media-libs/libpng, media-libs/jpeg-turbo, media-libs/tiff,
# kde-frameworks/kexiv2  are needed for non-autotests (not run by default).

libkipi_src_prepare() {
    cmake_src_prepare

    # The relevant code resides in the tests directory, but isn't run during
    # make check and also installs stuff. It's rather a sample implementation.
    # TODO: Fix this upstream
    option examples || \
        edo sed -e "/add_subdirectory(tests)/s:^:#:" -i CMakeLists.txt
}

libkipi_src_install() {
    cmake_src_install

    alternatives_for _${PN} ${SLOT} ${SLOT} \
        /usr/share/icons/hicolor/16x16/apps/kipi{,-${SLOT}}.png \
        /usr/share/icons/hicolor/22x22/apps/kipi{,-${SLOT}}.png \
        /usr/share/icons/hicolor/32x32/apps/kipi{,-${SLOT}}.png \
        /usr/share/icons/hicolor/48x48/apps/kipi{,-${SLOT}}.png \
        /usr/share/icons/hicolor/128x128/apps/kipi{,-${SLOT}}.png
}

libkipi_pkg_postinst() {
    alternatives_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

libkipi_pkg_postrm() {
    alternatives_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

