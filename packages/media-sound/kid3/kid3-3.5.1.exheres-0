# Copyright 2008, 2009, 2010, 2012, 2013, 2014, 2016 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2015 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake sourceforge [ suffix=tar.gz ]

SUMMARY="A KDE music tagger"
DESCRIPTION="
If you want to easily tag multiple MP3, Ogg/Vorbis, FLAC, MPC, MP4/AAC, MP2,
Speex, TrueAudio and WavPack files (e.g. full albums) without typing the same
information again and again and have control over both ID3v1 and ID3v2 tags,
then Kid3 is the program you are looking for.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    chromaprint [[ description = [ Support for import from MusicBrainz Fingerprint ] ]]
    id3         [[ description = [ Support for ID3v1 and ID3v2 tags ] ]]
"
DEPENDENCIES="
    build:
        app-text/docbook-xsl-stylesheets
        dev-lang/perl:*
        dev-libs/libxslt
        kde-frameworks/kdoctools:5
        chromaprint? ( virtual/pkg-config )
    build+run:
        kde-frameworks/kconfig:5
        kde-frameworks/kio:5
        media-libs/flac [[ note = [ Need Flac++, --enable-cxx. ] ]]
        media-libs/libogg
        media-libs/libvorbis
        media-libs/taglib[>=1.4]
        sys-libs/zlib
        x11-libs/qtbase:5[gui]
        x11-libs/qtdeclarative:5
        x11-libs/qtmultimedia:5
        x11-libs/qttools:5
        chromaprint? (
            media/ffmpeg
            media-libs/chromaprint
        )
        id3? ( media-libs/id3lib )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-3.4.3-Use-CMAKE_INSTALL_FULL_DATAROOTDIR.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DWITH_DBUS:BOOL=TRUE
    -DWITH_PHONON:BOOL=FALSE
    -DWITH_TAGLIB:BOOL=TRUE
    -DWITH_MP4V2:BOOL=FALSE
    # gstreamer can be used instead of ffmpeg for chromaprint decoding, seems pointless to add
    # options for that, so if chromaprint is enabled, force ffmpeg on
    -DWITH_GSTREAMER:BOOL=FALSE
    # QAudioDecoder from QtMultimedia[>=5] can also be used for chromaprint
    # decoding. Given that we build the KDE version we cannot use that with
    # KDE-4.x.
    -DWITH_QAUDIODECODER:BOOL=FALSE
    -DWITH_QT5:BOOL=TRUE
    -DWITH_APPS=KDE
    -DWITH_NO_MANCOMPRESS:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'id3 ID3LIB'
    'chromaprint CHROMAPRINT'
    'chromaprint FFMPEG'
    'chromaprint CHROMAPRINT_FFMPEG'
)

