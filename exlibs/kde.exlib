# Copyright 2008, 2009, 2010, 2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2014-2015, 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
#
# Use this exlib for all packages using KDE's CMake buildsystem

# Keep in sync with cmake.exlib in ::arbor
myexparam cmake_minimum_version=3.14

require flag-o-matic cmake [ cmake_minimum_version=$(exparam cmake_minimum_version) ]

myexparam -b debug=true
myexparam -b dependencies=true
# Set to '4' for packages based on kdelibs4 or '5' for packages based on
# extra-cmake-modules and possibly other frameworks.
myexparam kde_major_version=5
# Three possible values for translations. The first two options expect the
# needed files in po/.
# - qt:
#       Qt based translations
#       Typically those are recognised by something in CMakeLists.txt like:
#          "include (ECMPoQmTools)
#          ecm_install_po_files_as_qm(po)"
# - ki18n:
#       Gettext based translation via KDE's internationalization system
#       Those usually include:
#          "ki18n_install"
#       in CMakeLists.txt
# - none:
#       Default, no translations included.
myexparam translations=none

export_exlib_phases pkg_setup src_prepare src_configure

exparam -b debug &&
MYOPTIONS="debug"
exparam -v translation_type translations
exparam -v major_version kde_major_version

# This is a simplification, but probably every KDE project uses it, it's
# tiny with few dependencies and we usually only keep one Frameworks
# version (the latest) anyway. Avoids listing it everywhere or carrying
# an exparam with a mimimal version.
DEPENDENCIES="
    build:
        kde-frameworks/extra-cmake-modules[>=5.74.0]
"

# Translations aren't stored in the code repositories, but put together by
# various release scripts.
if ! ever is_scm ; then
    if [[ ${translation_type} == qt ]] ; then
        DEPENDENCIES+="
            build:
                x11-libs/qttools:5 [[ note = [ lrelease/lconvert for translations ] ]]
            "
    elif [[ ${translation_type} == ki18n && ${PN} != ki18n ]] ; then
        DEPENDENCIES+="
            build:
                kde-frameworks/ki18n:5 [[ note = [ ki18n_install() from KF5I18NMacros.cmake ] ]]
                sys-devel/gettext
        "
    fi
fi

CMAKE_SRC_CONFIGURE_TESTS+=( '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE' )

_remove_localisations_from_dir() {
    local dir="${1}" lingua

    for lingua in $(ls "${dir}") ; do
        if [[ -d ${dir}/${lingua} ]] && ! has ${lingua} ${LINGUAS} ; then
            # Remove localised files installed by ecm_install_po_file_as_qm(),
            # ki18n_install() and and kdoctools_install() called from the
            # parent CMakeLists.txt
            edo rm -rf "${dir}/${lingua}"

            # Remove localised data files installed via ordinary install in
            # a CMakeLists.txt
            if [[ -f ${dir}/CMakeLists.txt ]] ; then
                edo sed \
                    -e "/add_subdirectory[[:space:]]*([[:space:]]*${lingua}.*/d" \
                    -i "${dir}"/CMakeLists.txt
            fi
        fi
    done
}

kde_src_prepare() {
    cmake_src_prepare

    if ! ever is_scm ; then
        if [[ -n ${LINGUAS} ]] ; then
            echo "Removing unselected translations..."
            for dir in po poqm ; do
                if [[ -d ${CMAKE_SOURCE}/${dir} ]] ; then
                    _remove_localisations_from_dir ${dir}
                fi
           done
        fi
    fi
}

kde_pkg_setup() {
    exparam -b debug && option !debug && append-flags -DNDEBUG -DQT_NO_DEBUG
}

kf5_shared_cmake_params() {
    # The libexec, PLUGIN_INSTALL_DIR and QT_PLUGIN_INSTALL_DIR paths are
    # intentionally relative, as some paths are hardcoded into scripts or
    # programs. Giving absolute paths results in hardcoded paths like
    # /usr/usr/libexec, cf.
    # kde-modules/KDEInstallDirs.cmake in extra-cmake-modules
    echo \
        -DBUILD_SHARED_LIBS:BOOL=TRUE \
        -DKDE_INSTALL_AUTOSTARTDIR=/etc/xdg/autostart \
        -DKDE_INSTALL_CONFDIR=/etc/xdg \
        -DKDE_INSTALL_DATAROOTDIR:PATH=/usr/share \
        -DKDE_INSTALL_LIBEXECDIR=libexec \
        -DKDE_INSTALL_SYSCONFDIR=/etc \
        -DKDE_INSTALL_USE_QT_SYS_PATHS:BOOL=TRUE
}

kde_src_configure() {
    CMAKE_SRC_CONFIGURE_PARAMS+=( $(kf5_shared_cmake_params) )

    local p
    for p in ${MY_KDE_PARTS}; do
        CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( "kde_parts:${p}" )
    done

    cmake_src_configure
}

